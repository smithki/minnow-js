const Logger = require('achoo');
const chalk = require('chalk');

Logger.addTransform('cmd', str => chalk.magenta(str));
Logger.addTransform('file', str => chalk.cyan(str));
Logger.addTransform('important', str => chalk.red(str));
Logger.addTransform('env', str => chalk.green(str));

Logger.addTransform('type', (str, capitalize) => {
  if (capitalize) return str.replace(/^\w|\W/, match => match.toUpperCase());

  return str.toLowerCase();
});

function prettifyMillis(millis) {
  if (millis < 1) {
    return chalk.cyan(`${millis * 1000} μs`);
  } else if (millis > 1000) {
    const s = millis / 1000;
    const ms = prettifyMillis(s % 1);
    return chalk.cyan(`${Math.floor(s)} s ${ms} ms`);
  }
  return chalk.cyan(`${millis} ms`);
}

Logger.addTransform('dur', prettifyMillis);
