const Minnow = require('./Minnow');

const publicApi = {
  task(config) {
    Minnow.register('task', config);
  },

  sequence(config) {
    Minnow.register('sequence', config);
  },

  get tasks() {
    const result = {};

    Object.keys(Minnow.commands).forEach(cmd => {
      const taskObj = Minnow.commands[cmd];
      if (taskObj.getType() === 'task') result[cmd] = taskObj;
    });

    return result;
  },

  get sequences() {
    const result = {};

    Object.keys(Minnow.commands).forEach(cmd => {
      const sequenceObj = Minnow.commands[cmd];
      if (sequenceObj.getType() === 'sequence') result[cmd] = sequenceObj;
    });

    return result;
  },

  default(cmd) {
    Minnow.default = cmd;
  },

  run(cmd) {
    Minnow.run(cmd);
  },

  watch(pattern, cmd) {
    Minnow.watch(pattern, cmd);
  },
};

module.exports = publicApi;
