const Gaze = require('gaze');
const Logger = require('achoo');
const spawn = require('cross-spawn');
const fs = require('fs');
const which = require('npm-which')(fs.realpathSync(process.cwd()));
require('../achoo.transforms.js');

let instance;

class Minnow {
  constructor() {
    if (!instance) {
      instance = this;
    } else {
      return instance;
    }

    this.commands = {};
  }

  get default() {
    return this.defaultCmd;
  }

  set default(cmd) {
    this.defaultCmd = cmd;
  }

  register(type, config) {
    const cmd = config.name;

    if (!cmd) {
      Logger.error(
        { type: [type, true] },
        '<type> must have a defined `name`.',
      );
      process.exit(1);
    }

    if (this.commands[cmd]) {
      Logger.error({ cmd, type }, '<cmd> is already a registered <type>.');
      process.exit(1);
    }

    const requireDeps = () => {
      if (!config.deps) return undefined;

      const transformModuleName = moduleDep =>
        moduleDep.replace(/[-|.]+(\w|\W)/g, (match, group) =>
          group.toUpperCase(),
        );

      const nodeModules = {};

      config.deps.forEach(moduleDep => {
        const moduleName = transformModuleName(moduleDep);
        if (!nodeModules[moduleName]) {
          try {
            nodeModules[moduleName] = require(moduleDep);
            if (!nodeModules[moduleName]) {
              throw Error(
                `Node module ${moduleDep} not found, is it installed?`,
              );
            }
          } catch (err) {
            Logger.error(err.stack);
            process.exit(1);
          }
        }
      });

      return nodeModules;
    };

    const createCmd = () => {
      this.commands[cmd] = {};
      const newCmd = this.commands[cmd];

      newCmd.config = config;
      newCmd.profile = {
        startTime: undefined,
        timeElapsed: () => Date.now() - newCmd.profile.startTime,
      };

      newCmd.run = () => this.run(cmd);
      newCmd.getType = () => type;
      newCmd.prevEnv = undefined;

      if (type === 'task') {
        const taskArgs = {
          deps: requireDeps(),
          result: undefined,
        };

        newCmd.args = taskArgs;
      }

      return newCmd;
    };

    switch (type) {
      case 'task': {
        const newTask = createCmd();
        return newTask;
      }

      case 'sequence': {
        const newSequence = createCmd();
        return newSequence;
      }

      default: {
        Logger.error(
          { important: type },
          `<important> is not a valid registration options. Try 'task' or 'sequence'.`,
        );
        process.exit(1);
      }
    }

    return null;
  }

  run(cmd) {
    // return new Promise((resolve, reject) => {});
    const cmdObj = this.commands[cmd];

    if (!cmdObj) {
      Logger.error({ cmd }, '<cmd> is not a registered task or sequence.');
      process.exit(1);
    }

    this.preRun(cmdObj);

    if (cmdObj.config.run) this.handleTask(cmdObj);

    this.postRun(cmdObj);
  }

  preRun(cmdObj) {
    const profile = cmdObj.profile;
    profile.startTime = Date.now();

    const pre = cmdObj.config.pre;

    if (pre) {
      const preTasks = !Array.isArray(pre) ? [pre] : [...pre];

      preTasks.forEach(preTask => {
        this.run(preTask);
      });
    }

    const type = cmdObj.getType();
    const env = cmdObj.config.env;

    const enforceEnv = newEnv => {
      let prevEnv = cmdObj.prevEnv;

      if (prevEnv) prevEnv = process.env.NODE_ENV;

      process.env.NODE_ENV = newEnv;
    };

    let logEnvIfConfigured = '';

    if (env) {
      enforceEnv(env);
      logEnvIfConfigured = '<env>';
    }

    Logger.info(
      {
        type: [type, true],
        cmd: cmdObj.config.name,
        env: `NODE_ENV=${cmdObj.config.env || cmdObj.prevEnv}.`,
      },
      `<type> <cmd> started. ${logEnvIfConfigured}`,
    );
  }

  handleTask(cmdObj) {
    const task = cmdObj;
    const run = !Array.isArray(task.config.run)
      ? [task.config.run]
      : [...task.config.run];
    const type = task.getType();

    switch (type) {
      case 'task': {
        run.forEach(step => {
          switch (typeof step) {
            case 'string': {
              Minnow.handleSpawn(step, false);
              break;
            }

            case 'function': {
              task.args.result = step(task.args);
              break;
            }

            case 'object': {
              if (Array.isArray(step)) {
                Minnow.concurrentRun(step);
              } else {
                Logger.err(
                  { 'cmd@1': 'Object', 'cmd@2': task.config.name },
                  '<cmd@1> in <cmd@2> could not be executed.',
                );
                process.exit(1);
              }
              break;
            }

            default: {
              Logger.err('Oops, something went wrong...');
              process.exit(1);
              break;
            }
          }
        });
        break;
      }

      case 'sequence': {
        run.forEach(cmd => {
          this.run(cmd);
        });
        break;
      }

      default: {
        break;
      }
    }
  }

  postRun(cmdObj) {
    const env = cmdObj.config.env;
    let prevEnv = cmdObj.prevEnv;

    const revertEnv = () => {
      if (prevEnv) {
        process.env.NODE_ENV = prevEnv;
        prevEnv = undefined;
      }
    };

    if (env) revertEnv();

    Logger.info(
      {
        type: [cmdObj.getType(), true],
        cmd: cmdObj.config.name,
        dur: cmdObj.profile.timeElapsed(Date.now()),
      },
      '<type> <cmd> completed in <dur>.',
    );

    const post = cmdObj.config.post;

    if (post) {
      const postTasks = !Array.isArray(post) ? [post] : [...post];

      postTasks.forEach(postTask => {
        this.run(postTask);
      });
    }
  }

  watch(pattern, cmd) {
    Logger.info(
      {
        cmd,
        file: pattern,
      },
      'Changes to files matching <file> will execute <cmd>',
    );
    return new Gaze(pattern, (err, watcher) => {
      if (err) {
        Logger.error(err.stack);
        process.exit(1);
      }

      watcher.on('all', (event, file) => {
        if (event !== 'ready') {
          Logger.info(
            {
              file,
            },
            `<file> ${event}.`,
          );
          this.run(cmd);
        }
      });
    });
  }

  static handleSpawn(step, async) {
    const parts = step.split(' ');
    const CMD = which.sync(parts[0]);
    const PARAMS = parts.slice(1);
    const STDIO = { stdio: 'inherit' };

    if (async) {
      spawn(CMD, PARAMS, STDIO);
    } else {
      spawn.sync(CMD, PARAMS, STDIO);
    }
  }
}

module.exports = new Minnow();
