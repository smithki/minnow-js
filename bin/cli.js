#!/usr/bin/env node

'use-strict';

const yargs = require('yargs');
const fs = require('fs');
const path = require('path');
const tildify = require('tildify');
const Minnow = require('../lib/Minnow');
const Logger = require('achoo');

const argv = yargs
  .usage('Usage: $0 [options] [files]')
  .example(
    '$0 -e development my-task-or-sequence another-task',
    `Set \`process.env.NODE_ENV\` to "development", then run a task or sequence of tasks.`,
  )
  .options({
    env: {
      alias: 'e',
      describe: 'Set `process.env.NODE_ENV`.',
      type: 'string',
    },
    config: {
      alias: 'c',
      describe: 'Set path to your custom task file.',
      default: 'minnowfile.js',
      type: 'string',
    },
  })
  .help('help')
  .alias('help', 'h').argv;

process.env.NODE_ENV = argv.env;

const CWD = fs.realpathSync(process.cwd());
const CONFIG = {
  PATH: path.join(CWD, argv.config),
  BASENAME: path.basename(argv.config),
  get ABBR_PATH() {
    return tildify(this.PATH);
  },
};

if (fs.existsSync(CONFIG.PATH)) {
  require(CONFIG.PATH); //eslint-disable-line
  Logger.info(
    {
      file: CONFIG.ABBR_PATH,
      important: `NODE_ENV=${argv.env}`,
    },
    'Using configuration file <file>',
    argv.env ? 'with <important>' : '',
  );
} else {
  Logger.error(
    {
      file: CONFIG.ABBR_PATH,
      important: 'Could not find configuration file',
    },
    '<important> <file>',
  );
  process.exit(1);
}

if (argv._.length === 0) {
  try {
    if (Minnow.default) {
      argv._.push(Minnow.default);
    } else {
      throw new Error(
        {
          important: 'No default task is assigned.',
        },
        '<important>\n\nFor information about your available tasks, use the "--info" or "-i" option. To show help, use the "--help" or "-h" option.',
      );
    }
  } catch (err) {
    Logger.error(err.message);
    process.exit(1);
  }
}

argv._.forEach(cmd => {
  Minnow.run(cmd);
});
